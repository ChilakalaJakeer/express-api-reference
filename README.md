بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ

---

---

---

## Express api using mongoDB.Atlas

> Reference:
>
> - [ Writing a CRUD app with Node.js and MongoDB](https://codeburst.io/writing-a-crud-app-with-node-js-and-mongodb-e0827cbbdafb)
> - gitRepo for reference to [Folder structure](https://github.com/Eslamunto/ProductsApp) & [ MERN CRUD ](https://github.com/Eslamunto/ProductsApp)
>
> * Implementation of socket.io in MERN [article by Talha Muhhamad in Dev.to](https://dev.to/captainpandaz/a-socket-io-tutorial-that-isn-t-a-chat-app-with-react-js-58jh)
> * Add image upload

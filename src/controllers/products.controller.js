const Product = require("../models/products.model");

//To create product
exports.product_create = async (req, res, next) => {
  const product = new Product({
    productName: req.body.productName,
    productPrice: req.body.productPrice
  });
  //Allahamdulillah
  try {
    product
      .save()
      .then(() =>
        res.status(201).end("product added/created to DB successfully")
      );
  } catch (error) {
    res.status(422).json(err);
  }
};
//to read all products
exports.product_readAll = (req, res, next) => {
  Product.find()
    .then(products => {
      res.status(200).json(products);
    })
    .catch(err => {
      res.status(400).json(err);
    });
};
//to read by id
exports.product_readById = (req, res, next) => {
  Product.findById(req.params.id)
    .then(product => {
      res.status(200).json(product);
    })
    .catch(err => next(err));
};
//to update by id
exports.product_update = (req, res, next) => {
  Product.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    {
      new: true //to immedeatly get updated json in following res.json()
    }
  )
    .then(product => res.status(200).json(product))
    .catch(err => next(err));
};
//Mashallah
// to delete all products
exports.product_deleteAll = (req, res, next) => {
  Product.remove()

    .then(() => res.end("Deleted all products successfully"))
    .catch(err => next(err));
};

//to delete by id
exports.product_deleteById = (req, res, next) => {
  Product.findByIdAndRemove(req.params.id)
    .then(() =>
      res.end(
        "Mashallah,deleted one products with id :" +
          req.params.id +
          " successfully."
      )
    )
    .catch(err => next(err));
};

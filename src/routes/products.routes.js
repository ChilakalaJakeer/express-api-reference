const router = require("express").Router();
const products_controller = require("../controllers/products.controller");

router.post("/create", products_controller.product_create);

router.get("/", products_controller.product_readAll);

router.get("/:id", products_controller.product_readById);

router.put("/update/:id", products_controller.product_update);

router.delete("/delete", products_controller.product_deleteAll);

router.delete("/delete/:id", products_controller.product_deleteById);

module.exports = router;

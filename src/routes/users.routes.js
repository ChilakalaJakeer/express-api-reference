const router = require("express").Router();

const users_controller = require("../controllers/users.controller");

router.post("/register", users_controller.userRegister);
router.post("/login", users_controller.userLogin);

module.exports = router;

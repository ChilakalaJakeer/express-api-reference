// Allahamdulillah

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");
require("dotenv").config();

/* for .env  file
          
          PORT=
          MONGODB_URI=
          secretOrKey="secret"
*/
const Port = process.env.PORT || 5000;
const MongoDB =
  process.env.MONGODB_URI || "mongodb://localhost:27017/express-api-refence";
const app = express();
app.use(cors());
app.use(express.static("public/html"));
app.use(express.json());
mongoose.connect(MongoDB, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: true // to use update and delete by id controlls
});

mongoose.connection.once("open", () => {
  console.log("connected to mongoDB at ", MongoDB);
});

const productsRouter = require("./src/routes/products.routes");
app.use("/products", productsRouter);

//passportjs implementation
app.use(passport.initialize());

require("./src/passport");

const usersRouter = require("./src/routes/users.routes");
app.use("/users", usersRouter);

const server = app.listen(Port, () => {
  console.log("server running at port: ", Port);
});

const socketIo = require("socket.io");
//const server = require("./startServer");
const io = socketIo(server);

io.on("connection", socket => {
  console.log("made socket connection with Id :", socket.id);

  socket.on("products", data => {
    socket.broadcast.emit("updatedProducts", { updatedProducts: data });
  });
});
